const Generator = require('../../abstractGenerator');
const {randomise, unique} = require('../../helpers');
const {digits} = require('../../sets');

// https://de.wikipedia.org/wiki/Steuerliche_Identifikationsnummer#Aufbau

module.exports = class TIN extends Generator {
    generate(params = {}) {
        let rand = '';
        while ((rand[0] === '0') || !this._checkNumberOfUnique(rand)) {
            rand = randomise(digits, 10);
        }

        return `${rand.substr(0, 2)} ${rand.substr(2, 3)} ${rand.substr(5, 3)} ${rand.substr(8, 2)}${this._calculateChecksum(rand)}`;
    }

    validate(value) {
        value = value.replace(/[- ]/g,'');

        if (!value.match(/^\d{11}$/g)) {
            throw new Error('format')
        }

        if (value[0] === '0') {
            throw new Error('format')
        }

        const digits = value.substr(0, 10);
        if (!this._checkNumberOfUnique(digits)) {
            throw new Error('numberOfUniqueDigits')
        }

        if (this._calculateChecksum(digits) !== parseInt(value[10])) {
            throw new Error('checksum')
        }

        return true;
    }

    _calculateChecksum(digits) {
        let product = 10;
        for (let digit of digits) {
            let sum = (parseInt(digit) + product) % 10;
            if (sum === 0) { sum = 10; }
            product = (sum * 2) % 11;
        }

        return (11 - product) % 10;
    }

    _checkNumberOfUnique(digits) {
        if (digits.match(/(\d)\1\1/)) {
            return false;
        }

        const counts = new Array(10).fill(0);
        for (let digit of digits) {
            counts[parseInt(digit)]++;
        }
        counts.sort();

        return counts.join('') === '0111111112'
            || counts.join('') === '0011111113';
    }
};

// tests: 47313512808 -> invalid (doublt 3 and double 1)
