var Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/')
    .setPublicPath('/')
    .addEntry('app', './assets/js/app.js')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableBuildNotifications()
    .disableSingleRuntimeChunk()
    .enableSassLoader()
    .enablePostCssLoader()
    .copyFiles({
        from: './assets/static',
        to: '[path][name].[ext]',
    })
;

module.exports = Encore.getWebpackConfig();
