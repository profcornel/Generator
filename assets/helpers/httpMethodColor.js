module.exports = (name) => {
    name = name.toUpperCase();
    if (name === 'GET') {
        return 'success';
    } else if (name === 'POST') {
        return 'warning';
    } else if (name === 'PUT') {
        return 'info';
    } else if (name === 'DELETE') {
        return 'danger';
    }

    return 'secondary';
};
